Feature: To Verify that National Parcel locker option flows

  @sml @iSendParcels @National @ParcelLocker
  Scenario Outline: To Verify that the user is able to create a parcel with Parcel Locker option for National Country
    Given the user navigates to new SML application
    And User click on the Accept cookie
    And User click on Myaccount button
    And the user login to the application with username "vignesh.nagarajan@bpost.be" and password "@Vn875424"
    And the user clicks the Login button
    And User Click on send button
    And the user selects the Destiation of Parcel as "<Destination_Type>"
    And the user selects the weight option as "<Weight>"
    And Click on Continue button
    When the user enters the below Receiver details
      | FirstName   | LastName   | Company   | EmailAddress   | TelephoneNumber   |
      | <FirstName> | <LastName> | <Company> | <EmailAddress> | <TelephoneNumber> |
    And the user searches and selects the below Postalcode/City and Street in the receiver address page
      | Postalcode/City | Street |
      | 2850 Boom       | E19/A1 |
    And Click on Continue button
    And the user enters the Sender details
      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street     | Number |
      | Vignesh   | Nagarajan | Test    | vignesh@gmail.com |       456789645 | 2845 Niel       | BEEKSTRAAT | 12A    |
    And Click on Continue button
    Then verify whether the user is navigated to Basket page
    And the user sign out from the account

    Examples: 
      | Destination_Type | Weight  | FirstName | LastName | Company | EmailAddress     | TelephoneNumber |
      | Parcel locker    | 2-5 kg  | Ram       | Kumar    | Test    | Ram@gmail.com    |       456789645 |
      | Parcel locker    | 5-10 kg | Rajesh    | Kumar    | Test    | Rajesh@gmail.com |       456789645 |
