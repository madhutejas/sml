Feature: Verifying Sml details and Send Follow details

  #Background:
  Scenario: Verifying Sml login details with Valid details and Send label is added to the shopping basket
    Given User on Sml page
    When User click on the Accept cookie
    And User click on Myaccount button
    Then User should Enter testpost@post.com as username
    And User should Enter Test@123 as Password
    Then User Click the login button
    #Weight 0-2
    Given User Click on send button
    When User click on weight
    And Click on Continue Button
    Given Enter the User First Name and Last Name
    When Enter the user emailAddress
    And Enter the postal code and city
    Then Enter the street address
    And Click on the Continue button
    Given User Enter the Sender details in Firstname and Lastname
    When User Enter the Postal code and city
    And User enter the Street details
    Then User enter the Street Number
    And Enter the user Email address detaills
    Then click on the continue button
    #Given Click on the collect button
    #When Choose the Collection date
    #And Enter the Number of parcels, Total volume and Total weight
    #Then Click on Add to basket
    #Given Choose the Print label at bpost office
    #When Choose the Proof of payment
    #And Choose the terms and Conditions
    #Scenario: Verifying Sml login details with Valid details and Send label is added to the shopping basket
    #Weight 2-5
    Given Click on the Send button
    When Click on the Destination of the parcel
    And Click on the Weight
    Then Enter the Reference details
    And Click on the Additional options details
    Then Enter the email_id
    Then Click on Continue button
    Given Enter the user name
    When Enter the user email address
    And Enter The Pick up point details Postal code and city
    Then Enter The Pick up point street address
    And Click on the SML2 Continue button
    Given Enter the Sender details in Firstname and Lastname
    When Click the User Enter the Postal code and city
    And Enter the Street details
    Then Enter the Street Number
    And Enter the sender user Email address
    Then click on the Sm2 step2 continue button
    #Weight 5-10
    Given User Click on the Send button
    When User Click on the Destination of the parcel
    And User Click on the different Weight
    Then User Enter the Reference details
    And Click on the Recommended for you
    Then User Click on Continue button
    Given Enter the Receiver details in Firstname and Lastname
    When Select the Postal code and city
    And Select the Street details
    Then User Enter the Street Number in the receiver Page
    And Enter the sender user Email address for receiver
    And User Click on the SML3 Continue button for receiver
    Given User Enter the Sender details in Firstname and Lastname for sender details
    When User Click the User Enter the Postal code and city
    And User Enter the Street details
    Then User Enter the Street Number
    And User Enter the sender user Email address
    Then user click on the Sml3 step2 continue button
    #Weight 10-30
    Given User Click on send button in Sml page
    When User click on the diffrent weight
    And Click on Continue button in step1
    Given Enter the First Name and Last Name in the receiver
    When Enter the emailAddress in the receiver
    And Enter the postal code and city in the receiver
    Then Enter the street address in the receiver
    And Click on the Continue button in the receiver
    Given Enter the Sender details in Firstname and Lastname in the sender details
    When Enter the Postal code and city in the sender details
    And Enter the Street details in the sender details
    Then Enter the Street Number in the sender details
    And Enter the user Email address detaills in the sender details
    Then click on the continue button in the sender details
