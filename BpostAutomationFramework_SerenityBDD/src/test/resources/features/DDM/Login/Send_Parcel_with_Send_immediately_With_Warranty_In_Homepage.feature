Feature: To Verify that user is able to create Parcels with Send Immediately with Warranty options

  @sml @iSendParcels @National @ParcelLocker @sendImmediatelyWithWarranty
  Scenario: To Verify that the user is able to create a parcel with Parcel Locker option for National Country
    Given the user navigates to new SML application
    And User click on the Accept cookie
    And User click on Myaccount button
    And the user login to the application with username "vignesh.nagarajan@bpost.be" and password "@Vn875424"
    And the user clicks the Login button
    And the user clicks Send Immediately option with warranty option selected in the Homepage
    #And the user selects the Destiation of Parcel as "Parcel locker"
    And the user selects the weight option as "5-10 kg"
    And Click on Continue button
    When the user enters the below Receiver details
      | FirstName | LastName | Company | EmailAddress     | TelephoneNumber |
      | Rajesh    | Kumar    | Test    | Rajesh@gmail.com |       456789645 |
    And the user searches and selects the below Postalcode/City and Street in the receiver address page
      | Postalcode/City | Street |
      | 2850 Boom       | E19/A1 |
    And Click on Continue button
    And the user enters the Sender details
      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street          | Number |
      | Vignesh   | Nagarajan | Test    | vignesh@gmail.com |       456789645 | 2845 Niel       | NIEUW BEGIJNHOF | 12A    |
    And Click on Continue button
    Then verify whether the user is navigated to Basket page
    And the user selects the Print label option in the basket page
    And the user selects the Proof of payment option in the basket
    And the user clicks the Terms and conditions option and click the payment button
    And the user sign out from the account
