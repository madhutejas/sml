@TSP2-1270
Feature: AT_TC01_SML_iSendPracel_International_MyAddressBook

	@TEST_TSP2-1263
	Scenario: AT_TC01_SML_iSendPracel_International_MyAddressBook
		Given the user navigates to new SML application
		    And User click on the Accept cookie
		    And User click on Myaccount button
		    And the user login to the application with username and Password
		    And the user clicks the Login button
		    And User Click on MyAddress Book button
		    And User Click on Add address button in the Receiver
		    And The user enters the below ReceiverAddressdetails
		      | FirstName | LastName | Company | Country of destination | Street | Number | Box |Address |Address 2|Postal code|City |Email address								| 
		      | Ranjith   | Kumar    | infosys | Afghanistan            | Kabul  | 8      | 6001|Chaman  |Nandary  |1070,Kabul |Kabul|ranjith01kumar.r@gmail.com	|
		   And Click on the Save button
		   And User Click on Sender Address button
		   And User Click on Add address button in the Sender
		   And the user enters the SenderAddress details
		      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street     | Number |
		      | Ganesh    | Kumar     | Test    | ganesh@gmail.com  |       456789645 |  2845 Niel      | BEEKSTRAAT | 12A    |
		  And Click on the Check Box in the proof of payment Invoice
		  And Click on the Save button
		  And User Click on send button
		    And User Click on the Country of destination "Afghanistan"
		    And User enter the parcel summary in international country "SampleDetails" and "parcel details"
		    And User enter the item1 details in international country "Parcel description" and "2" and "13" and "450" and "india"
		    And Click on Continue button in international country
		    When the user enters the below Receiver details
		      | FirstName | LastName | Street | Number | PostalCode | City  | Email               |
		      | Malar   | Vasu     | Kabul  | 12-A   | 1002-Kabul | Kabul | malar@infosys.com |
		    And Click on Continue button
		     And the user enters the Sender details
		      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street     | Number |
		      | Vignesh   | Nagarajan | Test    | vignesh@gmail.com |       456789645 |  2845 Niel      | BEEKSTRAAT | 12A    |
		    And Click on the Continue
