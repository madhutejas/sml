Feature: To Verify that InterNational Parcel locker option flows

  Scenario: To Verify that the user is able to create a parcel with Parcel Locker option for InterNational Country
    Given the user navigates to new SML application
    And User click on the Accept cookie
    And User click on Myaccount button
    And the user login to the application with username and Password
    And the user clicks the Login button
    And User Click on send button
    And User Click on the Country of destination "Afghanistan"
    And User enter the parcel summary in international country "SampleDetails" and "parcel details"
    And User enter the item1 details in international country "Parcel description" and "2" and "13" and "450" and "india"
    And Click on Continue button in international country
    When the user enters the below Receiver details
      | FirstName | LastName | Street | Number | PostalCode | City  | Email               |
      | Ranjith   | Vasu     | Kabul  | 12-A   | 1002-Kabul | Kabul | ranjith@infosys.com |
    And Click on Continue button
     And the user enters the Sender details
      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street     | Number |
      | Vignesh   | Nagarajan | Test    | vignesh@gmail.com |       456789645 |  2845 Niel      | BEEKSTRAAT | 12A    |
    And Click on the Continue
    And User Click on send button
    And User Click on the Country of destination "Algeria"
    And User enter the parcel summary in international country "Reference" and "description"
    And User enter the item1 details in international country "Parcel description" and "1" and "10" and "350" and "Belgium"
    And Click on Continue button in international country
    When the user enters the below Receiver details
      | FirstName | LastName | Street | Number | PostalCode | City  | Email               |
      | Sulthan   | Khan     | Kabul  | 12-A   | 1002-Kabul | Kabul | sulthan@gmail.com |
    And Click on Continue button
     And the user enters the Sender details
      | FirstName | LastName  | Company | EmailAddress      | TelephoneNumber | Postalcode/City | Street     | Number |
      | Ganesh    | Kumar     | Test    | ganesh@gmail.com  |       456789645 |  2845 Niel      | BEEKSTRAAT | 12A    |
    And Click on the Continue
  	Then verify whether the user is navigated to Basket page  
    Then Click on the print label, Promocode, proof payment check box and Click on the Pay button
    Then Select Payment Method
    
    
