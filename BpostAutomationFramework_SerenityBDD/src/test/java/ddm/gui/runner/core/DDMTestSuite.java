package ddm.gui.runner.core;
import io.cucumber.core.snippets.SnippetType;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.aspectj.lang.annotation.After;
import org.jruby.RubyBoolean.False;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		
		plugin = {"pretty","html:target/site/cucumber-pretty","json:target/cucumber/cucumber.json"},
		
		//plugin = {"pretty","json:target/cucumber-reports/Cucumber.html"},
        features = "src/test/resources/features",
        		
        glue = "sml.gui.stepdefinitions",
        //glue = {"stepdefinitions"},
        tags = {"@sendImmediatelyWithWarranty"},
        monochrome = false,
        dryRun = false,
        snippets= io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE
        )
public class DDMTestSuite {

    @AfterClass
    public static void teardown() {
        System.out.println("Ran the after");
    }
}


