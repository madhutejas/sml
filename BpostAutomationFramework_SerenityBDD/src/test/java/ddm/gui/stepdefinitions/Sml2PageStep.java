package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.pages.Sml2Page;

public class Sml2PageStep {
	@Steps
	Sml2Page sml;
	
	@Given("Enter the User First Name and Last Name")
	public void enterTheUserFirstNameAndLastName() throws InterruptedException {
	  
		sml.firstNameLastName();
	}
	
	@When("Enter the user emailAddress")
	public void enterTheUserEmailAddress() throws InterruptedException {
	    
		sml.emailId();
	}
	@When("Enter the postal code and city")
	public void enterThePostalCodeAndCity() throws InterruptedException {
	    sml.postalCode();
	}
	@Then("Enter the street address")
	public void enterTheStreetAddress() throws InterruptedException {
	   sml.street();
	}
	
	@Then("Click on the Continue button")
	public void clickOnTheContinueButton() throws InterruptedException {
	    sml.SmlContinue();
	}
	
	
	
	
	
	
}
