package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.pages.Sml3Page;

public class Sml3PageStep {

	@Steps
	Sml3Page sml3;
	

@Given("User Enter the Sender details in Firstname and Lastname")
public void userEnterTheSenderDetailsInFirstnameAndLastname() throws InterruptedException 
{
    
	sml3.sml3FirstNameAndLastName();
}

@When("User Enter the Postal code and city")
public void userEnterThePostalCodeAndCity() throws InterruptedException 
{
   sml3.sml3PoctalCodeAndCity();
	
	
}
@When("User enter the Street details")
public void userEnterTheStreetDetails() throws InterruptedException {
   sml3.sml3Street();
}

@Then("User enter the Street Number")
public void userEnterTheStreetNumber() throws InterruptedException {
 
	sml3.sml3Number();

}
@Then("Enter the user Email address detaills")
public void enterTheUserEmailAddressDetaills() throws InterruptedException {
    
	sml3.sml3emailAddress();
}

@Then("click on the continue button")
public void clickOnTheContinueButton() throws InterruptedException {
    
	sml3.sml3Continue();
	
	
}























}
