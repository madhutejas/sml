package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.fourth.weight.SmlFourthWeight;

public class SmlFourthWeightStep {
	
	@Steps
	SmlFourthWeight s4;
	
	@Given("User Click on send button in Sml page")
	public void userClickOnSendButtonInSmlPage() {
		
		s4.send4();
	}

	@When("User click on the diffrent weight")
	public void userClickOnTheDiffrentWeight() throws InterruptedException {
		
		s4.weight4();
	
	
	}

	@When("Click on Continue button in step1")
	public void clickOnContinueButtonInStep1() throws InterruptedException {

		s4.ContinueButton4();
	}
	//-----------> Step 2 <----------------

	@Given("Enter the First Name and Last Name in the receiver")
	public void enterTheFirstNameAndLastNameInTheReceiver() throws InterruptedException {
	    
		s4.firstNameLastName4();
	}

	@When("Enter the emailAddress in the receiver")
	public void enterTheEmailAddressInTheReceiver() throws InterruptedException {

		s4.emailId4();
		
	}

	@When("Enter the postal code and city in the receiver")
	public void enterThePostalCodeAndCityInTheReceiver() throws InterruptedException {

		s4.postalCode4();
	}

	@Then("Enter the street address in the receiver")
	public void enterTheStreetAddressInTheReceiver() throws InterruptedException {

		s4.street4();
		
	}

	@Then("Click on the Continue button in the receiver")
	public void clickOnTheContinueButtonInTheReceiver() throws InterruptedException {

		s4.ContinueButton4();
	}

//---------------> Step 3 <----------------
	@Given("Enter the Sender details in Firstname and Lastname in the sender details")
	public void enterTheSenderDetailsInFirstnameAndLastnameInTheSenderDetails() throws InterruptedException {

		s4.firstNameAndLastNameSml4();
	}

	@When("Enter the Postal code and city in the sender details")
	public void enterThePostalCodeAndCityInTheSenderDetails() throws InterruptedException {
	    	s4.poctalCodeAndCitySml4();
	}

	@When("Enter the Street details in the sender details")
	public void enterTheStreetDetailsInTheSenderDetails() throws InterruptedException {

		s4.streetSml4();
	}

	@Then("Enter the Street Number in the sender details")
	public void enterTheStreetNumberInTheSenderDetails() throws InterruptedException {

		s4.numberSml4();
	}

	@Then("Enter the user Email address detaills in the sender details")
	public void enterTheUserEmailAddressDetaillsInTheSenderDetails() throws InterruptedException {

		s4.emailAddressSml4();
	}

	@Then("click on the continue button in the sender details")
	public void clickOnTheContinueButtonInTheSenderDetails() throws InterruptedException {

		s4.sml4ContinueSender();
		
	
	}

}
