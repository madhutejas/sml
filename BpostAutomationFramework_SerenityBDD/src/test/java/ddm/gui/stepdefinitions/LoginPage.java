package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.pages.HomePage;

public class LoginPage {
	@Steps
	HomePage home;
	
	
	@Given("User on Sml page")
	public void userOnSmlPage() {
		
		home.openTheApplication();
	   
	}
	@When("User click on the Accept cookie")
	public void user_click_on_the_Accept_cookie() {
		home.acceptCookies();
	
	}

	@When("User click on Myaccount button")
	public void user_click_on_Myaccount_button() throws InterruptedException {
	   home.clicktheMyacctout();
	}

	@When("User should Enter testpost@post.com as username")
	public void userShouldEnterTestpostPostComAsUsername() {
	    
	home.enterEmailAddress();
	
	}

	@When("User should Enter Test@{int} as Password")
	public void userShouldEnterTestAsPassword(Integer int1) {
	    
		home.enterPassword();
	}
	@Then("User Click the login button")
	public void userClickTheLoginButton() {
		home.clickTheSignOnbutton();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

//	@Then("Enter the Email address")
//	public void enterTheEmailAddress() {
//	    
//		home.emailAddress();
//	}
//	@Then("Enter the Postal Code and City")
//	public void enterThePostalCodeAndCity() {
//	   
//		home.postalCodeAndCity();
//
//	}
//
//	@Then("Click on the Continue button")
//	public void clickOnTheContinueButton() {
//	  
//		home.Continue();
//	}
//		
//	
//	
	

	
	
	
}
