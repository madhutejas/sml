package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.second.weight.SmlSecondWeight;

public class SmlSecondWeightstep {
	
	@Steps
	SmlSecondWeight s2;
	
	@Given("Click on the Send button")
	public void clickOnTheSendButton() {
	    
		s2.sendBtn2();
	}
	@When("Click on the Destination of the parcel")
	public void clickOnTheDestinationOfTheParcel() {
		s2.destOfTheParcel();

	}

	@When("Click on the Weight")
	public void clickOnTheWeight() throws InterruptedException {
		s2.secondWeight();

	}

	@Then("Enter the Reference details")
	public void enterTheReferenceDetails() {
		s2.reference();
		
		    }

	@Then("Click on the Additional options details")
	public void clickOnTheAdditionalOptionsDetails() {

		s2.additionalOptions();
		
	}
	@Then("Enter the email_id")
	public void enterTheEmail_id() throws InterruptedException {

		s2.email();
	}

	@Then("Click on Continue button")
	public void clickOnContinueButton() throws InterruptedException {
		s2.Sml2Cotinue();
	}
	
	@Given("Enter the user name")
	public void enterTheUserName() throws InterruptedException  {

		s2.s2firstNameLastName();
	}

	@When("Enter the user email address")
	public void enterTheUserEmailAddress() throws InterruptedException {

		s2.s2emailId();
	}

	@When("Enter The Pick up point details Postal code and city")
	public void enterThePickUpPointDetailsPostalCodeAndCity() throws InterruptedException {

		s2.s2postalCode();
	}

	@Then("Enter The Pick up point street address")
	public void enterThePickUpPointStreetAddress() throws InterruptedException {

		s2.s2street();
	}

	@Then("Click on the SML2 Continue button")
	public void clickOnTheSML2ContinueButton() throws InterruptedException {

		s2.s2SmlContinue();
	}
//--------> SML2 STEP 2 Page <-------------

	@Given("Enter the Sender details in Firstname and Lastname")
	public void enterTheSenderDetailsInFirstnameAndLastname() throws InterruptedException {
	   s2.s3FirstNameAndLastName();
	}

	@When("Click the User Enter the Postal code and city")
	public void clickTheUserEnterThePostalCodeAndCity() throws InterruptedException {

		s2.s3PoctalCodeAndCity();
	}

	@When("Enter the Street details")
	public void enterTheStreetDetails() throws InterruptedException {

		s2.s3Street();
	}

	@Then("Enter the Street Number")
	public void enterTheStreetNumber() throws InterruptedException {

		s2.s3Number();
	}

	@Then("Enter the sender user Email address")
	public void enterTheSenderUserEmailAddress() throws InterruptedException {

		s2.s3emailAddress();
	}

	@Then("click on the Sm2 step2 continue button")
	public void clickOnTheSm2Step2ContinueButton() throws InterruptedException {

		s2.s3Continue();
	}


}
