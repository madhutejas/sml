package ddm.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.second.weight.SmlSecondWeight;
import sml.third.weight.SmlThirdWeight;

public class SmlThirdWeightStep {
	
	@Steps
	SmlThirdWeight s3;

@Given("User Click on the Send button")
public void userClickOnTheSendButton() {
   s3.SendBtn3();
}

@When("User Click on the Destination of the parcel")
public void userClickOnTheDestinationOfTheParcel() {

	s3.destOfTheParcel3();
}

@When("User Click on the different Weight")
public void userClickOnTheDifferentWeight() throws InterruptedException {

	s3.thirdWeight();
}

@Then("User Enter the Reference details")
public void userEnterTheReferenceDetails() {

	s3.reference3();
}
@Then("Click on the Recommended for you")
public void clickOnTheRecommendedForYou() throws InterruptedException {

s3.recommendedForYou();
}

/*@Then("User Click on the Additional options details")
public void userClickOnTheAdditionalOptionsDetails() {

	s3.additionalOptions();
}
@Then("User Enter the email_id")
public void userEnterTheEmail_id() {

	s3.email3();
}
*/
@Then("User Click on Continue button")
public void userClickOnContinueButton() throws InterruptedException {

	s3.Sml3Cotinue();
}
//-------------> Step2 <---------------
@Given("Enter the Receiver details in Firstname and Lastname")
public void enterTheReceiverDetailsInFirstnameAndLastname() throws InterruptedException {

		s3.s3firstNameLastName();

}

@When("Select the Postal code and city")
public void selectThePostalCodeAndCity() throws InterruptedException {

	s3.s3postalCode();
}

@When("Select the Street details")
public void selectTheStreetDetails() throws InterruptedException {
	s3.s3street();
}

@Then("User Enter the Street Number in the receiver Page")
public void userEnterTheStreetNumberInTheReceiverPage() throws InterruptedException {
	 
	s3.s3Number();

}

@Then("Enter the sender user Email address for receiver")
public void enterTheSenderUserEmailAddressForReceiver() throws InterruptedException {

s3.s3emailAddress();
}

@Then("User Click on the SML3 Continue button for receiver")
public void userClickOnTheSML3ContinueButtonForReceiver() throws InterruptedException {
   s3.s3SmlContinuerece();
}
//----< Step 3<---------

@Given("User Enter the Sender details in Firstname and Lastname for sender details")
public void userEnterTheSenderDetailsInFirstnameAndLastnameForSenderDetails() throws InterruptedException {
    
	s3.Sml3FirstNameAndLastName();
}
@When("User Click the User Enter the Postal code and city")
public void userClickTheUserEnterThePostalCodeAndCity() throws InterruptedException {
   
	s3.Sml3PoctalCodeAndCity();
}

@When("User Enter the Street details")
public void userEnterTheStreetDetails() throws InterruptedException {

	s3.Sml3Street();
}

@Then("User Enter the Street Number")
public void userEnterTheStreetNumber() throws InterruptedException {

	s3.Sml3Number();
}

@Then("User Enter the sender user Email address")
public void userEnterTheSenderUserEmailAddress() throws InterruptedException {

	s3.Sml3emailAddress();
}

@Then("user click on the Sml3 step2 continue button")
public void userClickOnTheSml3Step2ContinueButton() throws InterruptedException {

	s3.Sml3Continue3();
}

}
