package api.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

//import io.cucumber.core.gherkin.messages.internal.gherkin.internal.com.eclipsesource.json.Json;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import api.templates.*;
import api.assertion.FormataddressTest;
import api.formataddress.*;

public class FormatUPUStepDefinitions {
	
	   @Steps
	    FormatUPURequest UPUReq;

	    @Steps
	    FormatUPUResponse UPURes;
	
	    @Steps
	    FormataddressTest Test;
	
	String FormatUPU,FormatUPU_exp;

	@Given("Send JSON request with request body")
	public void send_json_request_with_request_body(List<Map<String, String>> FormatDetails) throws IOException {
		
		FormatUPU = MergeFrom.template("templates/FormatUPU.json")
                .withDefaultValuesFrom(FieldValues.in("templates/standard-CSP.properties"))
                .withFieldsFrom(FormatDetails.get(0));
		
		System.out.println(FormatUPU);

	}

	@When("Check the status code of FormatUPUAddressesRequest")
	public void check_the_status_code_of_format_upu_addresses_request() {
		
		 FormatUPURequest.withDetails(FormatUPU);
	}

	@Then("Validate the response body:")
	public void validate_the_response_body(List<Map<String, String>> FormatExp) throws IOException {
		
        restAssuredThat(response -> response.statusCode(200));
        
		FormatUPU_exp = MergeFrom.template("templates/FormatUPU_Expected.json")
                .withDefaultValuesFrom(FieldValues.in("templates/standard-CSP-Exp.properties"))
                .withFieldsFrom(FormatExp.get(0));
		
		System.out.println(FormatUPU_exp);

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String ExpectedResponse = gson.toJson(FormatUPU_exp);

        JsonObject ActualResult = new Gson().fromJson(UPURes.Response(), JsonObject.class);
        
        JsonObject ExpectedResult = new Gson().fromJson(FormatUPU_exp, JsonObject.class);
        
        System.out.println("ActualResponse:"+ActualResult);
        
        Test.Verifyresponse(ActualResult,ExpectedResult);


	}
}
