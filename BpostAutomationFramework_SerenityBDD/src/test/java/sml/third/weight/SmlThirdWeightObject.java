package sml.third.weight;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SmlThirdWeightObject extends PageObject {
	
	public void SendBtn3()
	{
		$(By.xpath("(//*[contains(text(), 'Send')])[1]")).click();
	}
	public void destOfTheParcel3()
	{
		$(By.xpath("(//span[@class='destinationlabels-unchecked'])[2]")).click();
	}
	public void thirdWeight() throws InterruptedException
	{
		$(By.xpath("(//div[@class='form-group mb-0 weight-item-parcel'])[3]")).click();
		Thread.sleep(3000);
	}
	
	public void reference3()
	
	{
		$(By.id("senderReference")).type("testing reference");
		
	}
	
	public void recommendedForYou() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("//span[@class='recommendedlabel-unchecked']")).click();
	}
	
	/*public void additionalOptions()
	{
		//$(By.xpath("//span[@class='paymentOnDelivery-unchecked']")).click();
		$(By.xpath("//span[@class='confirmationOfReceipt-unchecked']")).click();
	}
	
	public void email3()
	{
		$(By.id("emailaddress")).type("ranjith1@gmail.com");
	}*/
	
	public void Sml3Cotinue() throws InterruptedException
	{
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(3000);
	}
	//---------> Step 2 <-----------------
	public void s3firstNameLastName() throws InterruptedException
	{
		$(By.id("first_name")).type("Liam");
		$(By.xpath("(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[1]")).type("kumar");
		Thread.sleep(2000);
	}

	public void s3postalCode() throws InterruptedException
	{
		
		WebElementFacade $txtEnterName = $(By.id("localityInputField"));
		$txtEnterName.type("26");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Thread.sleep(3000);

		
		
		Actions action =new Actions(getDriver());
		
		
		WebElementFacade $postal = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/bp-lib-address-autocomplete-by-component/div/bp-locality/div[1]/div[2]/div/ul/li[1]"));
		action.moveToElement($postal);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void s3street() throws InterruptedException
	{
		
		WebElementFacade $txtStreet = $(By.id("streetInputField"));
		$txtStreet.type("ACA");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $str = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li"));
		action.moveToElement($str);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
		}
	
	public void s3Number() throws InterruptedException 
	{
		$(By.id("streetNumberInputField")).type("234");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Number = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/bp-lib-address-autocomplete-by-component/div/bp-street-number/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Number);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	
	public void s3emailAddress() throws InterruptedException
	{
		Thread.sleep(2000);
		$(By.id("emailAddress")).type("Liam@rc.com");
		
	}
	public void s3SmlContinuerece() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(2000);
	}


//-------> Step 3 <----------
	
	public void Sml3FirstNameAndLastName() throws InterruptedException
	{
		WebElementFacade $name = $(By.id("first_name"));
		$name.clear();
		$name.type("john");
		Thread.sleep(2000);
		WebElementFacade $last = $(By.id("lastName"));
		$last.clear();
		$last.type("Noah");
				
		Thread.sleep(2000);
	}

	public void Sml3PoctalCodeAndCity() throws InterruptedException
	{
		
		WebElementFacade $sml3pocatl = $(By.id("localityInputField"));
		$sml3pocatl.clear();
		$sml3pocatl.type("Boom");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-locality/div[1]/div[2]/div/ul/li"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void Sml3Street() throws InterruptedException
	{
		
		WebElementFacade $sml3Street = $(By.id("streetInputField"));
		$sml3Street.clear();
		$sml3Street.type("Nete");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Click = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Click);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	
		}
	
	public void Sml3Number() throws InterruptedException 
	{
		WebElementFacade $sml3Street = $(By.id("streetNumberInputField"));
		$sml3Street.clear();
		$sml3Street.type("234");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Number = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street-number/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Number);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	
	public void Sml3emailAddress() throws InterruptedException
	{
		Thread.sleep(2000);
		WebElementFacade $sml3Street = $(By.id("emailAddress"));
				$sml3Street.clear();
				$sml3Street.type("Liam@rc.com");
		
	}
	public void Sml3Continue3() throws InterruptedException
	{
		Thread.sleep(4000);
		$(By.xpath("//*[contains(text(), 'Continue')]")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
	}
	
	

}
