package sml.gui.stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import sml.commons.SMLCommonObjects;
import sml.pages.BasketPage;
import sml.pages.SendPage;

public class BasketPageStep {

	@Steps
	BasketPage objBasketPage;
	
	@Steps
	SendPage objSendPage;
	
	
	@Then("the user clicks the edit icon in the receiver button and edits the Receiver details as below")
	public void theUserClicksTheEditIconInTheReceiverButtonAndEditsTheReceiverDetailsAsBelow(io.cucumber.datatable.DataTable dtReceiverDetails) throws InterruptedException {
	  objBasketPage.clickReceiverDetailsEdit();
	  Map<String,String> mapReceiverDetails = dtReceiverDetails.transpose().asMap(String.class, String.class);
	  objSendPage.enterReceiverDetails(mapReceiverDetails);
	}


	@Then("the user deletes the parcel from the basket")
	public void theUserDeletesTheParcelFromTheBasket() {
	    objBasketPage.clickRemoveParcelInBasket();
	}
	
	@Then("the user verifies that parcel is deleted and empty basket message is displayed in the basket")
	public void theUserVerifiesThatParcelIsDeletedAndEmptyBasketMessageIsDisplayedInTheBasket() throws InterruptedException {
	   assertEquals(SMLCommonObjects.strEmptyBasketMessage, objBasketPage.getBasketEmptyMessage());
	}
}
