package sml.gui.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import sml.pages.HomePage;


public class HomePageStep {

	@Steps
	HomePage objHomePage;
	

	
	@Given("the user navigates to new SML application")
	public void theUserNavigatesToNewSMLApplication() {
	    objHomePage.openTheApplication();
	}

	@When("User click on the Accept cookie")
	public void userClickOnTheAcceptCookie() {
	   objHomePage.acceptCookies();
	}

	@When("User click on Myaccount button")
	public void userClickOnMyaccountButton() throws InterruptedException {
	   objHomePage.clicktheMyacctout();
	}
	
	@Given("the user login to the application with username {string} and password {string}")
	public void theUserLoginToTheApplicationWithUsernameAndPassword(String strEmailAddress, String strPassword) {
		objHomePage.enterEmailAddress(strEmailAddress);
		objHomePage.enterPassword(strPassword);
	}

	@Given("the user clicks the Login button")
	public void theUserClicksTheLoginButton() {
	  objHomePage.clickTheSignOnbutton();
	}
	
	@Given("the user clicks Send Immediately option with warranty option selected in the Homepage")
	public void theUserClicksSendImmediatelyOptionWithWarrantyOptionSelectedInTheHomepage() {
	  objHomePage.clickSendImmediatelyWithWarranty();
	}

}
