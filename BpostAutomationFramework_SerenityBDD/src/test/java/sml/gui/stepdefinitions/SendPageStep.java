package sml.gui.stepdefinitions;

import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import sml.pages.SendPage;

public class SendPageStep {

	@Steps
	SendPage objSendPage;
	
	
	
	@Given("User Click on send button")
	public void userClickOnSendButton() {
	    objSendPage.send();
	}

	
	@Given("the user selects the Destiation of Parcel as {string}")
	public void theUserSelectsTheDestiationOfParcelAs(String strParcelDestination) throws InterruptedException {
	    objSendPage.selectDestinationOfTheParce(strParcelDestination);
	}
	@Given("the user selects the weight option as {string}")
	public void theUserSelectsTheWeightOptionAs(String strWeight) throws InterruptedException {
	   objSendPage.selectWeightOptionForTheParcel(strWeight);
	}

	@Given("Click on Continue button")
	public void clickOnContinueButton() throws InterruptedException {
	    objSendPage.ContinueButton();
	}
	
	@When("the user enters the below Receiver details")
	public void theUserEntersTheBelowReceiverDetails(DataTable dtReceiverDetails) throws InterruptedException {
	   
		Map<String,String> mapReceiverDetails = dtReceiverDetails.transpose().asMap(String.class, String.class);
		
		objSendPage.enterReceiverDetails(mapReceiverDetails);
	}
	
	
	@When("the user searches and selects the below Postalcode\\/City and Street in the receiver address page")
	public void theUserSearchesAndSelectsTheBelowPostalcodeCityAndStreetInTheReceiverAddressPage(io.cucumber.datatable.DataTable dtReceiverPostalCityAndStreet) throws InterruptedException {
		Map<String,String> mapPostalCodeAndStreetDetails = dtReceiverPostalCityAndStreet.transpose().asMap(String.class, String.class);
		objSendPage.enterPostalCodeAndStreet(mapPostalCodeAndStreetDetails);
	}
	
	@When("the user enters the Sender details")
	public void theUserEntersTheSenderDetails(io.cucumber.datatable.DataTable dtSenderDetails) throws InterruptedException {
		Map<String,String> mapSenderDetails = dtSenderDetails.transpose().asMap(String.class, String.class);
		
		objSendPage.enterSenderDetails(mapSenderDetails);
		
	}
	
	@Then("verify whether the user is navigated to Basket page")
	public void verifyWhetherTheUserIsNavigatedToBasketPage() throws InterruptedException {
	   assertTrue("Verifying whether User navigated to Basket page",objSendPage.verifyBasketPageTitle());
	   assertTrue("Verifying whether label is added to the Basket", objSendPage.verifyRecipientNameDisplayedInBasket());
	}
	
	@Then("the user sign out from the account")
	public void theUserSignOutFromTheAccount() throws InterruptedException {
	    objSendPage.signoutFromTheAccount();
	}
	
	@Then("the user selects the Print label option in the basket page")
	public void theUserSelectsThePrintLabelOptionInTheBasketPage() throws InterruptedException {
		objSendPage.clickPrintLabelOption();
	}

	@Then("the user selects the Proof of payment option in the basket")
	public void theUserSelectsTheProofOfPaymentOptionInTheBasket() throws InterruptedException {
		objSendPage.clickProofOfPaymentOption();
	}

	@Then("the user clicks the Terms and conditions option and click the payment button")
	public void theUserClicksTheTermsAndConditionsOptionAndClickThePaymentButton() throws InterruptedException {
		objSendPage.clickTermsAndConditionsAndCompletePayment();
	}
	@Given("User enter the parcel summary in international country {string} and {string}")
	public void userEnterTheParcelSummaryInInternationalCountryAnd(String strSummary, String strSummary1) throws InterruptedException, AWTException {
		objSendPage.parcelSummary(strSummary);
		objSendPage.parcelSummary1(strSummary1);
		objSendPage.parcelSummary2();
		
	}
	@Given("User enter the item1 details in international country {string} and {string} and {string} and {string} and {string}")
	public void userEnterTheItem1DetailsInInternationalCountryAndAndAndAnd(String strItem, String strItem1, String strItem2, String strItem3, String strItem4) throws InterruptedException, AWTException {
		objSendPage.itemDescription(strItem);
		objSendPage.itemDescription1(strItem1);
		objSendPage.itemDescription2(strItem2);
		objSendPage.itemDescription3(strItem3);
		objSendPage.itemDescription4(strItem4);
		
	}
	
}
