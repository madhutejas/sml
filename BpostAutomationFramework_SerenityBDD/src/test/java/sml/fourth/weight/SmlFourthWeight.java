package sml.fourth.weight;

import net.thucydides.core.annotations.Step;

public class SmlFourthWeight {
	
	
	SmlFourthWeightObject smlw4;
	
	@Step
	public void send4()
	{
		smlw4.send4();
	}
	@Step
	public void weight4() throws InterruptedException
	{
		smlw4.weight4();
	}
	@Step
	public void ContinueButton4() throws InterruptedException
	{
		smlw4.ContinueButton4();
	}
	
	///---------------> Step2 <-----------------
	
	@Step

	public void firstNameLastName4() throws InterruptedException
	{
		smlw4.firstNameLastName4();
	}
	@Step

	public void emailId4() throws InterruptedException
	{
		smlw4.emailId4();
	}
	@Step

	public void postalCode4() throws InterruptedException
	{
		smlw4.postalCode4();
	}
	@Step

	public void street4() throws InterruptedException
	{
		smlw4.street4();
	}
	
	@Step

	public void SmlContinue4() throws InterruptedException
	{
		smlw4.Continue4();
	}
	//-----------> Step3 <---------------------
	
	@Step
	public void firstNameAndLastNameSml4() throws InterruptedException
	{
		smlw4.firstNameAndLastNameSml4();
	}
	
	@Step
	public void poctalCodeAndCitySml4() throws InterruptedException
	{
		smlw4.poctalCodeAndCitySml4();
	}
	
	@Step
	public void streetSml4() throws InterruptedException
	{
		smlw4.streetSml4();
	}
	@Step
	public void numberSml4() throws InterruptedException
	{
		smlw4.numberSml4();
	}
	@Step
	public void emailAddressSml4() throws InterruptedException
	{
		smlw4.emailAddressSml4();
		
	}
	@Step
	public void sml4ContinueSender() throws InterruptedException
	{
		smlw4.sml4ContinueSender();
	}
	



}
