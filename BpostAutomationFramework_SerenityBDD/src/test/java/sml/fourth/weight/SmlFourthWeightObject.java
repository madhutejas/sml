package sml.fourth.weight;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SmlFourthWeightObject extends PageObject{
	
	public void send4()
	{
		$(By.xpath("(//*[contains(text(), 'Send')])[1]")).click();
	}
	
	public void weight4() throws InterruptedException
	
	{
		$(By.xpath("(//div[@class='form-group mb-0 weight-item-parcel'])[4]")).click();
		Thread.sleep(3000);
	}

	public void ContinueButton4() throws InterruptedException
	{
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(3000);
	}
	
	//------------------> Step 2 <------------------------
	
	public void firstNameLastName4() throws InterruptedException
	{
		$(By.id("first_name")).type("Alexander");
		Thread.sleep(1000);
		$(By.id("last_name")).type("Harper");
		Thread.sleep(1000);
		
	}
	
	public void emailId4() throws InterruptedException
	{
		$(By.id("emailAddress")).type("Harper@info.com");
		Thread.sleep(2000);
	}
	
	
	public void postalCode4() throws InterruptedException
	{
		WebElementFacade $txtEnterName = $(By.id("localityInputField"));
		$txtEnterName.type("2");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		
		Actions action =new Actions(getDriver());
		
		
		WebElementFacade $postal = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-locality/div/div[2]/div/ul/li[1]"));
		action.moveToElement($postal);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	public void street4() throws InterruptedException
	{
		WebElementFacade $txtStreet = $(By.id("streetInputField"));
		$txtStreet.type("A");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $str = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[1]"));
		action.moveToElement($str);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	public void Continue4() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(2000);
	}
	
	//-------------------> Step 3 <------------------------
	
	public void firstNameAndLastNameSml4() throws InterruptedException
	{
		$(By.id("first_name")).type("Liam");
		Thread.sleep(2000);
		$(By.id("lastName")).type("Noah");
		Thread.sleep(2000);
	}

	public void poctalCodeAndCitySml4() throws InterruptedException
	{
		
		WebElementFacade $sml3pocatl = $(By.id("localityInputField"));
		$sml3pocatl.type("b");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-locality/div[1]/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void streetSml4() throws InterruptedException
	{
		
		WebElementFacade $sml3Street = $(By.id("streetInputField"));
		$sml3Street.type("N");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Click = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3Click);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	
		}
	
	public void numberSml4() throws InterruptedException 
	{
		$(By.id("streetNumberInputField")).type("234");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Number = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street-number/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Number);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	
	public void emailAddressSml4() throws InterruptedException
	{
		Thread.sleep(2000);
		$(By.id("emailAddress")).type("Liam@rc.com");
		
	}
	public void sml4ContinueSender() throws InterruptedException
	{
		Thread.sleep(4000);
		$(By.xpath("//*[contains(text(), 'Continue')]")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
		Thread.sleep(4000);
		Thread.sleep(4000);
	}
	
	
	


}
