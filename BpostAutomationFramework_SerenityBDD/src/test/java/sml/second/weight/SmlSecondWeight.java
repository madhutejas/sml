package sml.second.weight;

import net.thucydides.core.annotations.Step;

public class SmlSecondWeight {

	SmlSecondWeightObject smlw;
	
	@Step
	public void sendBtn2()
	{
		smlw.SendBtn2();
	}
	@Step
	public void destOfTheParcel()
	{
		smlw.destOfTheParcel();
	}
	@Step
	public void secondWeight() throws InterruptedException
	{
		smlw.secondWeight();
	}
	@Step
	public void reference()
	{
		smlw.reference();
		
	}
	@Step
	public void additionalOptions()
	{
		smlw.additionalOptions();
	}
	
	@Step
	public void email() throws InterruptedException
	{
		smlw.email();
	}
	
	@Step
	public void Sml2Cotinue() throws InterruptedException
	{
		smlw.Sml2Cotinue();
	}
	public void s2firstNameLastName() throws InterruptedException
	{
		smlw.s2firstNameLastName();
	}
	@Step

	public void s2emailId() throws InterruptedException
	{
		smlw.s2emailId();
	}
	@Step

	public void s2postalCode() throws InterruptedException
	{
		smlw.s2postalCode();
	}
	@Step

	public void s2street() throws InterruptedException
	{
		smlw.s2street();
	}
	
	@Step

	public void s2SmlContinue() throws InterruptedException
	{
		smlw.s2Continue();
	}
//--------> Step3 <--------------
	
	@Step
	public void s3FirstNameAndLastName() throws InterruptedException
	{
		smlw.s3FirstNameAndLastName();
	}
	
	@Step
	public void s3PoctalCodeAndCity() throws InterruptedException
	{
		smlw.s3PoctalCodeAndCity();
	}
	
	@Step
	public void s3Street() throws InterruptedException
	{
		smlw.s3Street();
	}
	@Step
	public void s3Number() throws InterruptedException
	{
		smlw.s3Number();
	}
	@Step
	public void s3emailAddress() throws InterruptedException
	{
		smlw.s3emailAddress();
		
	}
	@Step
	public void s3Continue() throws InterruptedException
	{
		smlw.s3Continue();
	}
}
