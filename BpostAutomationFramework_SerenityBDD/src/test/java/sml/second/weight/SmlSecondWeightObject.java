package sml.second.weight;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SmlSecondWeightObject extends PageObject {
	
	public void SendBtn2()
	{
		$(By.xpath("(//*[contains(text(), 'Send')])[1]")).click();
	}
	public void destOfTheParcel()
	{
		$(By.xpath("(//div[@class='form-group col-sm-6 col-md-6'])[2]")).click();
	}
	public void secondWeight() throws InterruptedException
	{
		$(By.xpath("(//label[@class='shipping-method-labels weight-labels-unchecked'])[2]")).click();
		Thread.sleep(3000);
	}
	
	public void reference()
	
	{
		$(By.id("senderReference")).type("testing");
		
	}
	public void additionalOptions()
	{
		$(By.xpath("//span[@class='confirmationOfReceipt-unchecked']")).click();
	}
	public void email() throws InterruptedException
	{
		
		$(By.id("emailaddress")).type("ranjith1@gmail.com");
		Thread.sleep(3000);
	}
	
	public void Sml2Cotinue() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		
	}
	
//-------> SML2 Send Page <--------------------------
	
	public void s2firstNameLastName() throws InterruptedException
	{
		$(By.id("first_name")).type("William");
		Thread.sleep(1000);
		$(By.id("last_name")).type("James");
		Thread.sleep(1000);
		
	}
	
	public void s2emailId() throws InterruptedException
	{
		$(By.id("emailAddress")).type("James@info.com");
		Thread.sleep(2000);
	}
	
	
	public void s2postalCode() throws InterruptedException
	{
		WebElementFacade $txtEnterName = $(By.id("localityInputField"));
		$txtEnterName.type("2");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		
		Actions action =new Actions(getDriver());
		
		
		WebElementFacade $postal = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-locality/div/div[2]/div/ul/li[1]"));
		action.moveToElement($postal);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	public void s2street() throws InterruptedException
	{
		WebElementFacade $txtStreet = $(By.id("streetInputField"));
		$txtStreet.type("A");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $str = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[1]"));
		action.moveToElement($str);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	public void s2Continue() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(2000);
	}
	//----------> SML2 3 page <---------
	
	public void s3FirstNameAndLastName() throws InterruptedException
	{
		$(By.id("first_name")).type("Liam");
		Thread.sleep(2000);
		$(By.id("lastName")).type("Noah");
		Thread.sleep(2000);
	}

	public void s3PoctalCodeAndCity() throws InterruptedException
	{
		
		WebElementFacade $sml3pocatl = $(By.id("localityInputField"));
		$sml3pocatl.type("b");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-locality/div[1]/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void s3Street() throws InterruptedException
	{
		
		WebElementFacade $sml3Street = $(By.id("streetInputField"));
		$sml3Street.type("N");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Click = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3Click);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	
		}
	
	public void s3Number() throws InterruptedException 
	{
		$(By.id("streetNumberInputField")).type("234");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Number = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street-number/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Number);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	
	public void s3emailAddress() throws InterruptedException
	{
		Thread.sleep(2000);
		$(By.id("emailAddress")).type("Liam@rc.com");
		
	}
	public void s3Continue() throws InterruptedException
	{
		Thread.sleep(4000);
		$(By.xpath("//*[contains(text(), 'Continue')]")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
	}
	
	
}
