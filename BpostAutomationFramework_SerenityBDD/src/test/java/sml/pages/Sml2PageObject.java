package sml.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Sml2PageObject extends PageObject {
	
	public void firstNameLastName() throws InterruptedException
	{
		$(By.id("first_name")).type("William");
		Thread.sleep(1000);
		$(By.id("last_name")).type("James");
		Thread.sleep(1000);
		
	}
	
	public void emailId() throws InterruptedException
	{
		$(By.id("emailAddress")).type("James@info.com");
		Thread.sleep(2000);
	}
	
	
	public void postalCode() throws InterruptedException
	{
		WebElementFacade $txtEnterName = $(By.id("localityInputField"));
		$txtEnterName.type("2");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		
		Actions action =new Actions(getDriver());
		
		
		WebElementFacade $postal = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-locality/div/div[2]/div/ul/li[1]"));
		action.moveToElement($postal);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	public void street() throws InterruptedException
	{
		WebElementFacade $txtStreet = $(By.id("streetInputField"));
		$txtStreet.type("A");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $str = $(By.xpath("/html/body/app-root/app-receiver-detail/div[2]/div/div[3]/div[1]/div/form/div[1]/div[3]/div[1]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[1]"));
		action.moveToElement($str);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	public void Continue() throws InterruptedException
	{
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
		Thread.sleep(2000);
	}
	
	

}
