package sml.pages;

import net.thucydides.core.annotations.Step;

public class BasketPage {

	BasketPageObject objBasketPage;
	
	
	@Step
	public void clickReceiverDetailsEdit() {
		objBasketPage.clickReceiverDetailsEdit();
	}
	
	@Step
	public void clickRemoveParcelInBasket() {
		objBasketPage.clickRemoveButton();
		objBasketPage.clickYesOnDeleteLabelOverlay();
	}
	
	@Step
	public String getBasketEmptyMessage() throws InterruptedException {
		return objBasketPage.getBasketEmptyMessage();
	}
}
