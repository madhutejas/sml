package sml.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Sml3PageObject extends PageObject {
	
	public void sml3FirstNameAndLastName() throws InterruptedException
	{
		$(By.id("first_name")).type("Liam");
		Thread.sleep(2000);
		$(By.id("lastName")).type("Noah");
		Thread.sleep(2000);
	}

	public void sml3PoctalCodeAndCity() throws InterruptedException
	{
		
		WebElementFacade $sml3pocatl = $(By.id("localityInputField"));
		$sml3pocatl.type("b");
		Thread.sleep(3000);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-locality/div[1]/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void sml3Street() throws InterruptedException
	{
		
		WebElementFacade $sml3Street = $(By.id("streetInputField"));
		$sml3Street.type("N");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Click = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street/div/div[2]/div/ul/li[2]"));
		action.moveToElement($sml3Click);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	
		}
	
	public void sml3Number() throws InterruptedException 
	{
		$(By.id("streetNumberInputField")).type("234");
		Thread.sleep(3000);
		Thread.sleep(3000);
		
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3Number = $(By.xpath("/html/body/app-root/app-sender-detail/div[2]/div/div[3]/div[1]/form/div[1]/div[4]/bp-lib-address-autocomplete-by-component/div/bp-street-number/div/div[2]/div/ul/li"));
		action.moveToElement($sml3Number);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
		
	}
	
	
	public void sml3emailAddress() throws InterruptedException
	{
		Thread.sleep(2000);
		$(By.id("emailAddress")).type("Liam@rc.com");
		
	}
	public void sml3Continue() throws InterruptedException
	{
		Thread.sleep(4000);
		$(By.xpath("//*[contains(text(), 'Continue')]")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
