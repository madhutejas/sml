package sml.pages;

import java.util.concurrent.TimeUnit;

//import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.WebDriverWait;

//import ddm.gui.assertion.HomePageTest;
//import io.restassured.http.Cookies;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
//import net.serenitybdd.core.pages.WebElementFacade;


public class HomePageObjects extends PageObject{
	
	
	
	public void acceptCookie() {
		
		getDriver().manage().window().maximize();
		WebElement cookie = getDriver().findElement(By.xpath("(//*[contains(text(),'Accept cookies')])[2]"));
		if(cookie.getSize().height>0 && cookie.getSize().width>0) {
			cookie.click();
		}
		
		
	}
	
	
	public void clickTheMyAccount() throws InterruptedException {
		
		$(By.id("openModalButton")).click();
	}
	public void enterEmailAddress()
	{
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		$(By.id("username")).type("testpost@post.com");
		
	}
	
	public void enterEmailAddress(String strEmailAddress) {
		$(By.id("username")).sendKeys(strEmailAddress);
	}
	

	public void enterPassword() {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		$(By.id("password")).type("Test@123");
		
	}
	
	public void enterPassword(String strPassword) {
		$(By.id("password")).sendKeys(strPassword);
	}
	
	public void clickTheSignOnButton() {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		$(By.xpath("//a[@title='Sign in']")).click();
	}
	
	public void clickSendImmediatelyWithWarranty() {
		WebElement sendImmediately = getDriver().findElement(By.xpath("//h4[text()=' With warranty ']/following::a[contains(.,'Send immediately')]"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("arguments[0].scrollIntoView(true);", sendImmediately);
		sendImmediately.click();
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	
//	public void emailAddress()
//	{
//	
//		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//		$(By.id("emailAddress")).type("ranjith@info.com");
//	}
//	
//
//	
//	public void postalCodeAndCity()
//	{
//		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//		$(By.id("localityInputField")).type("2240 Massenhoven");
//		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//		$(By.xpath("(//*[contains(text(),' ZANDHOVEN')])[2]")).click();
//		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//
//		$( By.xpath("(//*[contains(text(),'Send a parcel')])[1]")).click();
//		
//		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		$(By.id("streetInputField")).type("Burgenland");
//	}
//	
//	public void Continue()
//	
//	{
//		
//		$(By.xpath("//button[@class='btn btn-large btn-block submit-button button-enabled']")).click();
//	}
//	
//	
//	
//	
//	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	WebElementFacade Webelement;
//	
//	HomePageTest Test;
//	 	
//	
//	
//	
//	
//	public void ddm_username() {
//		
//		System.out.println("Step1");
//		//$(By.id(""))
//		
//		$(By.name("q")).typeAndEnter("bpost");
//		
//	}
//
//	public void ddm__password() {
//		System.out.println("Step2");
//		
//	}
//	public void ddm_loginclick() {
//		
//		//Webelement.findElement(By.xpath("//button[contains(text(),'Submit')]")).sendKeys(Keys.ENTER);
//	}
//	
//	public void ddm_demandscreen() {
//		
//		System.out.println("Step3");
//		
//		//String  username= $(By.xpath("//*[@id=\"navbarCollapse\"]/ul[2]/li[6]/a/span[2]")).getText();
//		
//		//Test.Verifyname("u515097", "u515098");
//		
//		
//	}
}

