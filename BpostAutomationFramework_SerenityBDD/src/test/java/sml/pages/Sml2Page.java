package sml.pages;

import net.thucydides.core.annotations.Step;

public class Sml2Page {
	
	Sml2PageObject sml;
	@Step

	public void firstNameLastName() throws InterruptedException
	{
		sml.firstNameLastName();
	}
	@Step

	public void emailId() throws InterruptedException
	{
		sml.emailId();
	}
	@Step

	public void postalCode() throws InterruptedException
	{
		sml.postalCode();
	}
	@Step

	public void street() throws InterruptedException
	{
		sml.street();
	}
	
	@Step

	public void SmlContinue() throws InterruptedException
	{
		sml.Continue();
	}
	

}
