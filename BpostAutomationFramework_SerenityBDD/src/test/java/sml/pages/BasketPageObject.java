package sml.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

public class BasketPageObject extends PageObject {

	public void clickSenderDetailsEdit() {
		WebElement btnSenderDetailsEdit = $(By.xpath("//label[text()='Sender']/following-sibling::a"));
		btnSenderDetailsEdit.click();
	}
	
	public void clickReceiverDetailsEdit() {
		WebElement btnReceiverDetailsEdit = $(By.xpath("//label[text()='Receiver']/following-sibling::a"));
		btnReceiverDetailsEdit.click();
	}
	
	public void clickRemoveButton() {
		WebElement btnRemove = $(By.xpath("//h5[contains(.,'Remove')]"));
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", btnRemove);
	}
	
	public void clickYesOnDeleteLabelOverlay() {
		WebElement btnYesInDeleteLabelOverlay = $(By.xpath("//div[contains(@style,'display: block;')]//div[@class='delete-modal-error-buttons']//button[text()='Yes']"));
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript("arguments[0].click();", btnYesInDeleteLabelOverlay);
	}
	
	public String getBasketEmptyMessage() throws InterruptedException {
		Thread.sleep(2000);
		WebElement msgBasketEmpty = $(By.xpath("(//form//label)[1]"));
		return msgBasketEmpty.getText();
	}
}
