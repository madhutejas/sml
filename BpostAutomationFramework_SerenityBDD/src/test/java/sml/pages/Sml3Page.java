package sml.pages;

import net.thucydides.core.annotations.Step;

public class Sml3Page {
	
	Sml3PageObject sml3;
	
	@Step
	public void sml3FirstNameAndLastName() throws InterruptedException
	{
		sml3.sml3FirstNameAndLastName();
	}
	
	@Step
	public void sml3PoctalCodeAndCity() throws InterruptedException
	{
		sml3.sml3PoctalCodeAndCity();
	}
	
	@Step
	public void sml3Street() throws InterruptedException
	{
		sml3.sml3Street();
	}
	@Step
	public void sml3Number() throws InterruptedException
	{
		sml3.sml3Number();
	}
	@Step
	public void sml3emailAddress() throws InterruptedException
	{
		sml3.sml3emailAddress();
		
	}
	@Step
	public void sml3Continue() throws InterruptedException
	{
		sml3.sml3Continue();
	}
	

}
