package sml.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import ch.qos.logback.core.joran.action.Action;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SendPageObject extends PageObject {
	
	String strDestinationOfParcel = "//label[text()='Destination of the parcel']/following::label[contains(.,'$DestinationOfParcel')]";
	//String strWeightOptionOfTheParcel = "//p[text()='$WeightOfTheParcel']/following-sibling::input";
	//String strWeightOptionOfTheParcel  = "//label[contains(.,'$WeightOfTheParcel')]";
	
	
	public void send()
	{
		$(By.xpath("(//*[contains(text(), 'Send')])[1]")).click();
	}
	
	public void weight() throws InterruptedException
	
	{
		$(By.xpath("(//div[@class='form-group mb-0 weight-item-parcel'])[1]")).click();
		Thread.sleep(3000);
	}

	public void ContinueButton() throws InterruptedException
	{
		//((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]"));
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,250)", "");
		Thread.sleep(3000);
		$(By.xpath("(//div[@class='col-md-6 col-sm-12'])[2]")).click();
	
	}
	
	public void clickDestinationOfParcel(String strDestination) throws InterruptedException {
		strDestinationOfParcel=strDestinationOfParcel.replace("$DestinationOfParcel", strDestination);
		Thread.sleep(3000);
		$(By.xpath(strDestinationOfParcel)).click();
		
	}
	
	public void clickWeightOfTheParcel(String strWeightOfTheParcel) throws InterruptedException {
		String strWeightOptionOfTheParcel  = "//label[contains(.,'$WeightOfTheParcel')]";
		strWeightOptionOfTheParcel = strWeightOptionOfTheParcel.replace("$WeightOfTheParcel", strWeightOfTheParcel);
		Thread.sleep(2000);
		$(By.xpath(strWeightOptionOfTheParcel)).click();
	}
	
	
	public void enterReceiverFirstName(String strReceiverFirstName) throws InterruptedException {
		Thread.sleep(2000);
		WebElementFacade firstNameElement = $(By.xpath("//label[text()='First name']/following-sibling::input"));
		firstNameElement.clear();
		firstNameElement.type(strReceiverFirstName);
		
	}
	
	public void enterReceiverLastName(String strReceiverLastName) {
		WebElementFacade lastNameElement = $(By.xpath("//label[text()='Last name']/following-sibling::input"));
		lastNameElement.clear();
		lastNameElement.type(strReceiverLastName);
	}
	
	public void enterReciverEmailAddress(String strReceiverEmail) {
		WebElementFacade emailAddressElement = $(By.xpath("//label[contains(text(),'Email address')]/following-sibling::input"));
		emailAddressElement.clear();
		emailAddressElement.type(strReceiverEmail);
	}
	
	public void enterReceiverCompanyName(String strCompany) {
		WebElementFacade companyNameElement = $(By.xpath("//label[contains(text(),'Company ')]/following-sibling::input"));
		companyNameElement.clear();
		companyNameElement.type(strCompany);
	}
	
	public void enterReceiverTelephoneNumber(String strTelephoneNumber) {
		WebElementFacade telephoneNumberElement = $(By.xpath("//input[contains(@name,'phoneNumber')]"));
		telephoneNumberElement.clear();
		telephoneNumberElement.type(strTelephoneNumber);
	}
	
	
	public void selectReceiverPoctalCodeAndCity(String strPostalCode) throws InterruptedException
	{
		Thread.sleep(2000);
		WebElementFacade $sml3pocatl = $(By.id("localityInputField"));
		$sml3pocatl.clear();
		$sml3pocatl.type(strPostalCode);
		Thread.sleep(5000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("//ul[@id='bpaac-locality-suggestionsList']//li"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		
		Thread.sleep(3000);
	}
	
	public void selectReceiverStreet(String strStreet) throws InterruptedException {
		Thread.sleep(2000);
		WebElementFacade streetElement = $(By.xpath("//input[contains(@id,'streetInputField')]"));
		Thread.sleep(4000);
		streetElement.clear();
		streetElement.type(strStreet);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		//WebElementFacade $sml3p = $(By.xpath("//ul[@id='bpaac-street-suggestionsList']//li"));
		
		WebElementFacade $sml3p = $(By.xpath("//*[@id='bpaac-street-suggestionsList']/li[1]"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
	public void selectStreetNumbert(String strStreetNumber) throws InterruptedException {
		Thread.sleep(2000);
		WebElementFacade streetElement = $(By.xpath("//input[contains(@id,'streetNumberInputField')]"));
		streetElement.clear();
		Thread.sleep(2000);
		streetElement.type(strStreetNumber);
		Thread.sleep(3000);
		Actions action =new Actions(getDriver());
		WebElementFacade $sml3p = $(By.xpath("//ul[@id='bpaac-streetNb-suggestionsList']//li"));
		action.moveToElement($sml3p);
		action.doubleClick().build().perform();
		Thread.sleep(3000);
	}
	
   public String getBasketPageTitle() {
	   String strTitle = $(By.xpath("//div[@class='parcel-detail']//h3[text()='Basket']")).getText();
	   return strTitle;
   }
   
   public boolean verifyRecipientNameDisplayedInBasket(String strRecipientName) throws InterruptedException {
	   String recipientNameObj = "//label[@class='recipient-name-basket' and contains(.,'$RecipientName')]";
	   recipientNameObj = recipientNameObj.replace("$RecipientName", strRecipientName);
	   Thread.sleep(2000);
	   return $(By.xpath(recipientNameObj)).isDisplayed();
   }
   
   public void signOutFromTheAccount() throws InterruptedException {
	   Thread.sleep(2000);
	   $(By.xpath("//a[@id='openModalButton']")).click();
	   Thread.sleep(2000);
	   $(By.xpath("//a[text()=' Sign out ']")).click();
	   Thread.sleep(2000);
   }
   
   public void clickPrintLabelOption() throws InterruptedException {
	   WebElement click =$(By.xpath("//input[@name='printLabel']"));
	   JavascriptExecutor executor = (JavascriptExecutor)getDriver();
	   executor.executeScript("arguments[0].click();", click);
	   Thread.sleep(1500);
   }
	
   public void clickProofOfPayment() throws InterruptedException {
	   
	   WebElement proofOfPayment = $(By.xpath("//input[@name='proofOfPaymentOrInvoice']"));
	   JavascriptExecutor executor = (JavascriptExecutor)getDriver();
	   executor.executeScript("arguments[0].click();", proofOfPayment);
	   Thread.sleep(1500);
   }
   
   public void clickTermsAndConditionAndPay() throws InterruptedException {
	   WebElement termsAndCondtions= $(By.xpath("//input[@name='confirmcheck']"));
	   JavascriptExecutor executor3 = (JavascriptExecutor)getDriver();
	   executor3.executeScript("arguments[0].click();", termsAndCondtions);
	   Thread.sleep(1000);
	   
	   WebElement payButton = $(By.xpath("//button[text()='Pay']"));
	   executor3.executeScript("arguments[0].click();", payButton);
	   Thread.sleep(1000);
   }
   
   public void selectPaymentMethodAndCompletesAuthentication() throws InterruptedException {
	   WebElement paymentMethod= $(By.xpath("//*[contains(text(),'KBC')]"));
	   JavascriptExecutor executor = (JavascriptExecutor)getDriver();
	   executor.executeScript("arguments[0].click();", paymentMethod);
	   Thread.sleep(1500);
	   WebElement testButton = $(By.xpath("(//*[contains(text(),'TEST')])"));
	   executor.executeScript("arguments[0].click();", testButton);
	   Thread.sleep(3000);
   }
   public void parcelSummary(String strSummary) throws InterruptedException 
	{				
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(500); 
		$(By.id("senderReference")).sendKeys(strSummary);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(500); 
	}
	public void parcelSummary1(String strSummary1) throws InterruptedException {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElementFacade $content=$(By.xpath("//input[@name='contentDescription']"));
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		$content.sendKeys(strSummary1);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	public void parcelSummary2() throws InterruptedException, AWTException {
		WebElementFacade $shipType=$(By.xpath("//span[@class='k-input']"));
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		$shipType.click();
		Robot robot = new Robot(); 
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);	
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}
	public void itemDescription(String strItem) throws InterruptedException, AWTException {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(500); 

		$(By.id("content_description")).sendKeys(strItem);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(500); 
	}
	
		public void itemDescription1(String strItem1) throws InterruptedException {

		$(By.id("no_items")).sendKeys(strItem1);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(1000);
		}
		public void itemDescription2(String strItem2) throws InterruptedException {
		$(By.id("weight_kg")).sendKeys(strItem2);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(1000);
		}
		public void itemDescription3(String strItem3) throws InterruptedException {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		$(By.id("parcel_value")).sendKeys(strItem3);
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(1000);
		}
		public void itemDescription4(String strItem4) throws AWTException, InterruptedException {
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		$(By.xpath("(//input[@class='k-input'])[2]")).sendKeys(strItem4);
		Robot robot = new Robot(); 
		robot.keyPress(KeyEvent.VK_DOWN);
       robot.keyPress(KeyEvent.VK_ENTER);	
       
		Thread.sleep(1000);


	}
	
	
	
	
	
	
	
	
	
	
}
