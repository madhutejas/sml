package sml.pages;

import java.awt.AWTException;
import java.util.Map;

import net.thucydides.core.annotations.Step;
import sml.commons.SMLCommonObjects;

public class SendPage {
	
	SendPageObject send1;
	@Step
	public void send()
	{
		send1.send();
	}
	@Step
	public void weight() throws InterruptedException
	{
		send1.weight();
	}
	@Step
	public void ContinueButton() throws InterruptedException
	{
		send1.ContinueButton();
	}
	
	
	@Step
	public void selectDestinationOfTheParce(String strParcelDestination) throws InterruptedException {
		send1.clickDestinationOfParcel(strParcelDestination);
	}
	
	@Step
	public void selectWeightOptionForTheParcel(String strParcelWeight) throws InterruptedException {
		send1.clickWeightOfTheParcel(strParcelWeight);
	}
	
	@Step
	public void enterReceiverDetails(Map<String, String> mapReceiverDetails) throws InterruptedException {
		SMLCommonObjects.mapRecipientDetails = mapReceiverDetails; 
		send1.enterReceiverFirstName(mapReceiverDetails.get("FirstName"));
		send1.enterReceiverLastName(mapReceiverDetails.get("LastName"));
		send1.enterReceiverCompanyName(mapReceiverDetails.get("Company"));
		send1.enterReciverEmailAddress(mapReceiverDetails.get("EmailAddress"));
		send1.enterReceiverTelephoneNumber(mapReceiverDetails.get("TelephoneNumber"));
	}
	
	@Step
	public void enterPostalCodeAndStreet(Map<String, String> mapPostalCodeAndStreetDetails) throws InterruptedException {
		send1.selectReceiverPoctalCodeAndCity(mapPostalCodeAndStreetDetails.get("Postalcode/City"));
	}
	
	@Step
	public void enterSenderDetails(Map<String, String> mapSenderDetails) throws InterruptedException {
		send1.enterReceiverFirstName(mapSenderDetails.get("FirstName"));
		send1.enterReceiverLastName(mapSenderDetails.get("LastName"));
		send1.enterReceiverCompanyName(mapSenderDetails.get("Company"));
		send1.selectReceiverPoctalCodeAndCity(mapSenderDetails.get("Postalcode/City"));
		send1.selectReceiverStreet(mapSenderDetails.get("Street"));
		send1.selectStreetNumbert(mapSenderDetails.get("Number"));
		send1.enterReciverEmailAddress(mapSenderDetails.get("EmailAddress"));
		send1.enterReceiverTelephoneNumber(mapSenderDetails.get("TelephoneNumber"));
	}
	
	@Step
	public boolean verifyBasketPageTitle() {
		return "Basket".equals(send1.getBasketPageTitle());
	}
	
	@Step
	public boolean verifyRecipientNameDisplayedInBasket() throws InterruptedException {
		return send1.verifyRecipientNameDisplayedInBasket(SMLCommonObjects.mapRecipientDetails.get("FirstName"));
	}
	
	@Step
	public void signoutFromTheAccount() throws InterruptedException {
		send1.signOutFromTheAccount();
	}
	
	@Step
	public void clickPrintLabelOption() throws InterruptedException {
		send1.clickPrintLabelOption();
	}
	
	@Step
	public void clickProofOfPaymentOption() throws InterruptedException {
		send1.clickProofOfPayment();
	}
	
	@Step
	public void clickTermsAndConditionsAndCompletePayment() throws InterruptedException {
		send1.clickTermsAndConditionAndPay();
		send1.selectPaymentMethodAndCompletesAuthentication();
	}
	
	@Step
	public void parcelSummary(String strSummary) throws InterruptedException 
	{
		send1.parcelSummary(strSummary);
	}
	@Step
	public void parcelSummary1(String strSummary1) throws InterruptedException 
	{
		send1.parcelSummary1(strSummary1);
	}
	@Step
	public void parcelSummary2() throws InterruptedException, AWTException 
	{
		send1.parcelSummary2();
	}
	@Step
	public void itemDescription(String strItem) throws InterruptedException, AWTException
	{
		send1.itemDescription(strItem);
	}
	@Step
	public void itemDescription1(String strItem1) throws InterruptedException, AWTException
	{
		send1.itemDescription1(strItem1);
	}
	@Step
	public void itemDescription2(String strItem2) throws InterruptedException, AWTException
	{
		send1.itemDescription2(strItem2);
	}
	@Step
	public void itemDescription3(String strItem3) throws InterruptedException, AWTException
	{
		send1.itemDescription3(strItem3);
	}
	@Step
	public void itemDescription4(String strItem4) throws InterruptedException, AWTException
	{
		send1.itemDescription4(strItem4);
	}
	
	
	
	
	
	
	
	
	
}


